﻿namespace nilnul.set
{
	/// <summary>
	/// in memory of DesCarte, here Carte means the product of two, or a number of sets.
	/// </summary>
	/// <remarks>
	/// Vs: <see cref="IDes"/> which means consideration, be it product or not, of two sets, whileas this means product.
	/// </remarks>
	public interface ICarte { }
}
