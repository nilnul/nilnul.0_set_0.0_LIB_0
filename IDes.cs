﻿namespace nilnul.set
{
	/// <summary>
	/// in memory of DesCarte, here des means <see cref="nilnul.obj.IDuo"/>, that is, taking two in default, or a number of sets in consideration;
	/// </summary>
	/// <remarks>
	/// Vs: <see cref="ICarte"/> which means product, whileas this means only consideration, not specifically product;
	/// </remarks>
	public interface IDes { }
}
